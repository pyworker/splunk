# coding=utf-8
"""
модуль формирует job (поисковый запрос) и запускает его
выполнение в Splunk дожидается окончание запущенного job
и выводит в консоль результат выполнения job
"""

from time import sleep
import splunk
from otcommon.wing import Wing
import otcommon.splunklib.client as client
# библиотрка для получения результата выполнения запроса
import otcommon.splunklib.results as results


if __name__ == '__main__':

    # инициализация подключения к Splunk
    wing = Wing()
    session_key = wing.read()
    # для записи в индекс требуется инициализировать в Splunk сессионный ключ
    splunk.setDefault('sessionKey', session_key)

    # подключение к Splunk
    service = client.connect(token=session_key)

    # формирование поискового запроса для Splunk (1)
    simple_query = (
        'search index=_internal | head 1'
    )

    # формирование поискового запроса для Splunk (2)
    complex_query = (
        '| makeresults | sendemail message="%s" subject="%s" to="%s" priority="%s"'
        % ('email_message', 'email_subject', 'artem@ot.ru', 'hight')
    )

    # выполнение поискового запроса
    kw = {"exec_mode": "normal"}
    job = service.jobs.create(complex_query, **kw)

    # вывод информации об идентификаторе (sid) выполняемого job
    print(job.sid)

    # ожидание окончания выполнения задания
    while not job.is_done():
        sleep(.2)
    print("job is done")

    # вывод результата поискового запроса
    rr = results.ResultsReader(job.results())
    for result in rr:
        if isinstance(result, results.Message):
            # Diagnostic messages may be returned in the results
            print '%s: %s' % (result.type, result.message)
        elif isinstance(result, dict):
            # Normal events are returned as dicts
            print result
