# coding=utf-8
"""
модуль выполняющий поисковый запрос в KV Store splunk
"""

import json
import splunk
from otcommon.wing import Wing
import otcommon.splunklib.client as client


if __name__ == '__main__':
    # инициализация подключения к Splunk
    wing = Wing()
    session_key = wing.read()
    # для записи в индекс требуется инициализировать в Splunk сессионный ключ
    splunk.setDefault('sessionKey', session_key)

    service = client.connect(
            token=session_key,
            apps='alert_manager',
            owner='nobody'
    )

    # вывести список доступных collection (KV Store)
    print "KV Store Collections:"
    for collection in service.kvstore:
        print "  %s" % collection.name

    # удалить collection с именем collection_name (если она есть)
    collection_name = "example_collection"
    if collection_name in service.kvstore:
        service.kvstore.delete(collection_name)

    # создадаим коллекцию с именем collection_name
    service.kvstore.create(collection_name)

    # выведем содержимое коллекции
    collection = service.kvstore[collection_name]
    print "Should be empty: %s" % json.dumps(collection.data.query())

    # добавим содержимое к созданную коллекцию
    collection.data.insert(json.dumps({"_key": "item1", "somekey": 1, "otherkey": "foo"}))
    collection.data.insert(json.dumps({"_key": "item2", "somekey": 2, "otherkey": "foo"}))
    collection.data.insert(json.dumps({"somekey": 3, "otherkey": "bar"}))

    # Let's make sure it has the data we just entered
    print "Should have our data: %s" % json.dumps(collection.data.query(), indent=1)

    # поиск записи в KVStore по идентификатору
    print "Should return item1: %s" % json.dumps(collection.data.query_by_id("item1"), indent=1)

    # выполним поисковый запрос
    query = json.dumps({"otherkey": "foo"})
    print "Should return item1 and item2: %s" % json.dumps(collection.data.query(query=query), indent=1)

    # удалить коллекцию
    collection.delete()
