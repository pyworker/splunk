# coding=utf-8
"""
модуль записывает информацию в индекс Splunk
"""

import splunk
import socket
import splunk.input as input
from otcommon.wing import Wing
import otcommon.splunklib.client as client

INDEXNAME = 'alerts'


def attach_index(index):
    """
    функция записи в индекс с использованием функции attach
    :param index: дескриптор индекса в который выполняется запись
    :return:
    """
    mysocket = index.attach(
        sourcetype='my_sourcetype',
        host=socket.gethostname()
    )

    mysocket.send('attach_index: Hello from attach_index')
    mysocket.close()


def attach_index_with(index):
    """
    функция записи в индекс с использованием with attach
    :param index: дескриптор индекса в который выполняется запись
    :return:
    """
    with index.attached_socket(
            sourcetype='my_sourcetype',
            host=socket.gethostname()) as s:
        s.send("attach_index_with: Hello from attach_index_with\\r\\n")


def attach_index_file(index):
    """
    функция записив индекс информации с использованием файла
    :param index: дескриптор индекса в который выполняется запись
    :return:
    """
    # отслеживает изменился ли файл с предыдущей записи,
    # если да, то записывает весь файл, если нет, то не записывает
    index.upload('C:/project/_prog/Python/test_jane/splunk_test/test')


def clean_index(index, time):
    """
    функция очистки содержимого индекса
    :param index: дескриптор индекса, записи которого удаляютяс
    :param time: временной интервал, за который события нужно удалить из индекса [cек]
    :return:
    """
    index.clean(timeout=time)


def submit_index(index_name):
    """
    функция записи в индекс информации о событии
    ОСОБЕННОСТЬ: не предназначен для записи длинных строк (обрезает их)
    :param index_name: название индекса в который осуществляется запись события
    :return:
    """
    server_response = input.submit(
        'submit_index: test-submit="data input"',
        hostname=socket.gethostname(),
        sourcetype='my_sourcetype',
        source='my_source.py',
        index=index_name
    )
    print server_response
    return True if int(server_response.get('status')) == 200 else False


if __name__ == '__main__':

    # инициализация подключения к Splunk
    wing = Wing()
    session_key = wing.read()
    # для записи в индекс требуется инициализировать в Splunk сессионный ключ
    splunk.setDefault('sessionKey', session_key)

    service = client.connect(token=session_key)

    # дескриптор индекса с коктрым предстоит рпботать
    index = service.indexes[INDEXNAME]

    attach_index(index)
    attach_index_with(index)
    attach_index_file(index)
    submit_index(INDEXNAME)
    # clean_index(index, 30)
