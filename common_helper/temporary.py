"""
module for working with temporary files
necessary for testing modules
"""

import os
import tempfile

def create_tmp():
    """
    create, write, read and delete tmp file
    Raises:
        OSError: failed to create
               : failed to delete
    Return:
        True if file created, read\wrote and deleted
    """
    try:
        with tempfile.NamedTemporaryFile(delete=False) as fp:
            fp.write(b'Hello world!')
            fp.seek(0)
            fp.read()
            return True
    except OSError as os_err:
        raise OSError('failed to create tmp file: %s' % fp.name) 
    finally:
        os.remove(fp.name)
        print('here')
        if os.path.exists(fp.name):
            raise OSError('failed to delete tmp file: %s' % fp.name)


if __name__ == '__main__':
    create_tmp()
