# v.1.3
# coding=utf-8

import os
import py_compile
import re
import tarfile

# список разрешений файлов, версию которых требуется контролировать 
EXT_FILE_VERSION = ['.py']
RELEASE_VERSION = '.releaseversion'
RELEASE_IGNORE = '.releaseignore'

VERSION_PYTHONSTRING = '# v.%s'

# путь где будет создана директория, содержащая последний релиз
RELEASE_DIRECTORY = '../var/releases'


def update_version(version, lavel='bottom'):
    """
    функция обновления (инкрементирования) текущей версии

    :param version: вересия скрипта
    :type version: `String`

    :param lavel: уровень изменения версии
    :type lavel: `String`

    :return новая (инкрементированная) версия
    :rtype: `String`
    """

    if lavel and lavel not in ['top', 'bottom']:
        raise ValueError('error="set error level %s"' % lavel)
    try:
        v = version.split('.')
        v[-1] = str(int(v[-1]) + 1)
        version = '.'.join(v)
    except ValueError as val_err:
        raise ValueError('error="cant update current version %s"' % e)
    return version


def get_version():
    """
    функция получения текущей версии релиза 
    """
    try:

        if not os.path.exists(RELEASE_VERSION):
            with open(RELEASE_VERSION, 'w') as f:
                f.write('1.0')

        with open(RELEASE_VERSION, 'r+') as f:
            # инкрементируем версию и записываем в файлы
            version = update_version(f.read())
            print('current release version=%s' % version)
            f.seek(0)
            f.write(version)
    except IOError as io_err:
        raise io_err

    return version


def set_version(filename, version):
    """
    функция высталения в файл текущей версии релиза
    вресия релиза хранится в файлу RELEASE_VERSION
    
    :param filename: название фйала, в который записывается версия релиза
    :type `String`

    :param version: версия, которую нужно записать в файл
    :type version: `String`

    return: результат добавления версии в файл
    rtype: `Boolean`
    """

    if not filename or not os.path.exists(filename):
        raise IOError('error="cant find file with name=%s"' % filename)

    # паттерн для поиска строки с версией в файле
    pattern = re.compile('#.v\.')
    version_str = VERSION_PYTHONSTRING % version

    # открытие файла и запись строки релиза
    try:
        with open(filename, 'r+b') as f:
            data = f.read()

            change_flag = False

            for line in data.split('\n'):
                if pattern.match(line):
                    if line.strip() == version_str:
                        print('same version in file="%s"' % filename)
                        change_flag = True
                        break
                    else:
                        print('change version in file="%s"' % filename)
                        data = data.replace(line, version_str)
                        change_flag = True
                        break

            if not change_flag:
                print('add version in file="%s"' % filename)
                data = '\n'.join([version_str, data])

            f.seek(0)
            f.write(data)
    except IOError as io_err:
        raise io_err


def get_all_files(extension, path='.'):
    """
    функция обхода всех файлов все каталогов

    :param path: путь до каталога
    :type path: `String`

    :param extension: разрешения файлов, которые требуется найти
    :type extension: `List`
    
    :raises Exception: ошибка при обходе файлов 

    :return список найденых файлов
    :trype `List` 
    """
    if not len(extension):
        raise ValueError('error="zero length extension list"')

    if not os.path.exists(path):
        raise Exception('error="no such path=%s"' % path)

    if not isinstance(extension, list):
        extension = extension.split()

    version_files = []
    try:
        for path, subdir, files in os.walk(path):
            for file in files:
                # поиск всех файлов с требуемым расширением 
                filename, file_extension = os.path.splitext(file)
                if file_extension in extension:
                    version_file = os.path.join(path, file)
                    version_files.append(version_file)

    except Exception as err:
        raise err

    return version_files


def commit_changes(commit_message=None):
    """
    функция фиксации изменений в репозитории
    :param commit_message: сообщение сопутствующее фиксации изменений
    :return:
    """
    pass
    if not commit_message:
        pass


def create_app_archive(version, output_filename=None):
    # считываем .releaseignore
    if os.path.exists(RELEASE_IGNORE):
        print('find "%s"' % RELEASE_IGNORE)
        with open(RELEASE_IGNORE, 'r') as f:
            ignore = f.read()
        ignore = ignore.split('\n')
    else:
        print('cant find "%s"' % RELEASE_IGNORE)
        ignore = []

    # формируем список директорий, которые должны быть в приложении
    # TODO неправильно берется имя приложения исмправить
    app_name = os.path.basename(os.getcwd())
    # files = os.listdir('..')

    files = get_all_files(extension=['.py', '.json'], path='..')
    print(files)

    # имя архива
    if not output_filename:
        # формирование имени архива
        version_str = VERSION_PYTHONSTRING % version
        output_filename = '%s_%s.tar.gz' % (app_name, version_str[1:])

    # создать папку release_v.1.0
    if not os.path.exists(RELEASE_DIRECTORY):
        os.mkdir(RELEASE_DIRECTORY)
        print 'create release directory'
    else:
        print 'find release directory'

    with tarfile.open(os.path.join(RELEASE_DIRECTORY, output_filename), "w:gz") as tar:
        for file in files:
            if file in ignore:
                print('ignore file with name "%s"' % file)
                continue

            # удаление всех диреткорий содержащих в названии точку
            if os.path.isdir(file) and '.' in file:
                continue
            tar.add(file, arcname=os.path.join(app_name, file))


def compile_file(filename):
    """
    функция копиляции python скриптов
    """
    print(py_compile.compile(filename))


def change_config():
    """
    функция изменения конфигурационных файлов с
    develop среды на production
    """
    pass


if __name__ == '__main__':
    try:
        version = get_version()
        for file in get_all_files(extension=[".py"], path='..'):
            set_version(file, version)
            print(file)
        # формирование архива приложения 
        create_app_archive(version)

    except Exception as e:
        print(e)
        print('error with set version in file="%s"' % e)

