"""
modle for create path (python > 2.5)
"""

from errno import EEXIST
from os import makedirs, path

def create_path(dir_path):
    """
    create new path 
    if path already exist: ignore
    Args:
        path: path for new dir
    Raises:
        Exception: failed to create path
    Returns:
        True if path created
    """
    try:
        makedirs(dir_path)
    except OSError as os_err:
        if os_err.errno == EEXIST and path.isdir(dir_path):
            # path already exist
            # ignore
            pass
        else:
            raise Exception('failed to create path %s: %s' % (dir_path, os_err))
    return True

if __name__ == '__main__':
    create_path('D:/prog/python/project/splunk/common_helper/test')
