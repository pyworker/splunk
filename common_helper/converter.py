import csv

def csv_to_dict(csv_text):
    """
    convert csv-text to python dictionary
    Args:
        csv_text: str, string in csv format
    Raises:
    Returns:
        list of dictionaries 
    """
    
    iterable_text = csv_text.split('\n')
    reader = csv.DictReader(iterable_text, delimiter = ',', quotechar = '"')
    res = []
    for line in reader:
        res.append(dict(line))
    return res

