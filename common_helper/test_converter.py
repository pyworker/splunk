import os
import tempfile
import unittest

from converter import csv_to_dict

CSV_EXAMPLE = (
    'Lead,Title,Phone,Notes\n'
    'Jim Grayson,Senior Manager,(555)761-2385,"Spoke Tuesday, he`s interested"\n'
)

DICT_EXAMPLE = {'Lead': 'Jim Grayson', 'Title': 'Senior Manager', 'Phone': '(555)761-2385', 'Notes': 'Spoke Tuesday, he`s interested'}


class TestConverter(unittest.TestCase):
    """docstring for TestConverter"""
    def test_currect_convert(self):
        try:
            # prepare csv file before test
            with tempfile.NamedTemporaryFile(delete=False) as fp:
                fp.write(bytes(CSV_EXAMPLE, encoding='utf8'))

            # read csv file
            with open(fp.name, mode='r', encoding='utf8') as f: # try to use "rb" mode for read
                text = f.read()

            ans = csv_to_dict(csv_text=text)
            self.assertDictEqual(ans[0], DICT_EXAMPLE)

        finally:
            # delete tmp file
            os.remove(fp.name)
            
if __name__ == '__main__':
    unittest.main()        
