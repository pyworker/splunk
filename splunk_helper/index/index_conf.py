"""
module for configuration index
"""

# index for storing information
index = 'alerts'

# used splunkd management port
port = 8089
