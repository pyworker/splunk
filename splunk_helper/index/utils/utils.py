# v.1.3
# coding: utf-8
"""
module with auxiliary functions using Splunk-SDK
"""

import splunk
from otcommon.wing import Wing


def get_splunk_session_key():
    """
    function to get a new session key
    :return: new session_key
    :rtype: `String`
    """
    # initializing the connection to the Splunk
    wing = Wing()
    session_key = wing.read()
    # initialization of the session key in the Splunk
    splunk.setDefault('sessionKey', session_key)
    return session_key


