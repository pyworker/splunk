# v.1.3
# coding: utf-8
"""
модуль со вспомогательными функциями
"""
from datetime import datetime
from time import mktime


def time_converter(stime):
    """
    перевод указанного пользоватем времени в секунды

    :param stime: время указанное пользователем в конфигурационном файле
    :rtype `String`

    :return: время в секундах
    """
    time_modifiers = {'s': 1, 'm': 60, 'h': 3600, 'd': 86400, 'w': 604800}

    if stime.isdigit():
        time_modifier = 's'
        time_range = int(stime)
    else:
        time_modifier = stime[-1]
        time_range = int(stime[:-1])

    return time_range * time_modifiers[time_modifier]


def date_time_to_epoch():
    """
    функция вывода временной метки в формате datetime и epoch
    :return: None
    """
    timestamp = datetime.now()
    epoch_timestamp = mktime(timestamp.timetuple())
    iso_timestamp = str((datetime.now().isoformat()))
    print 'current time %s' % timestamp
    print 'current time in iso %s' % iso_timestamp
    print 'current time in epoch %s' % epoch_timestamp


if __name__ == '__main__':
    tmp_timestamp = '12h'
    res = time_converter(tmp_timestamp)
    print res


