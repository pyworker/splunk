"""
module for interaction with the Splunk index

require:
    utils.get_splunk_session_key    
"""

import socket
import splunk
import splunk.input as input
import otcommon.splunklib.client as client

from utils import get_splunk_session_key
# local config file
from index_conf import port, index 


def attach_index(data, index_info=None):
    """
    write information to the Splunk index via http-stream
    Args:
        data: str, information
        index: str, index name (extra param)
    Raises:
        Exception: failed to connect to Splunk
                 : cant attached to socket
        KeyError: invalid index
    Returns:
        True if data wrote to index
        False if data didnt write to index
    """
    #
    # connect to Splunk
    #
    try:
        session_key = get_splunk_session_key()
        service = client.connect(token=session_key, port=port)
    except Exception as ex:
        raise Exception('failed to write data to index: no connections to Splunk: %s' % ex)
    #
    # get Splunk index name
    #
    if index_info:
        name = index_info
    else:
        if index:
            name =  index
        else:
            raise Exception('failed to write data to index: cant find index name')
    #
    # write data to Splunk index
    #
    try:
        index = service.indexes[name]
    except KeyError as key_err:
        raise KeyError('failed to write data to index: invalid index name: %s' % key_err.message)    
    try:
        with index.attached_socket(
                sourcetype='incident_change',
                source='alert_manager_filereader.py',
                host=socket.gethostname()) as s:
            # the number of characters written in the index
            server_response = s.send(data)
    except Exception as ex:
        raise KeyError('failed to write data to index: cant attached to socket %s' % ex)
    
    if not server_response:
        return False
    return True


def submit_index(data, index_info=None):
    """
    write information to the index via submit
    Args:
        data: str, information 
        mindex: str, index name (extra param)
    Raises:
        Exception: failed to write data to index
    Returns:
        True
    """

    #
    # get index name
    #
    if index_info:
        index_name = index_info
    else:
        if index:
            index_name = index
        else:
            raise Exception('failed to write data to index: cant find index name')
    #
    # write data to Splunk index
    #
    try:
        server_response = input.submit(
            data,
            hostname=socket.gethostname(),
            sourcetype='test_sourcetype',
            source='test_source',
            index=index_name
        )
    except splunk.RESTException as ex_rest:
        raise Exception('failed to write data to index: %s' % ex_rest.message)

    return True if int(server_response.get('status')) == 200 else False


def create_job(spl_query):
    """
    creating and executing a query to the Splunk index
    Args:
        spl_query: splunk query for run job (in spl)
    Raises:
        Exception: failed to run job
    Return:
    """
    # TODO: session key is field of class index
    session_key = get_splunk_session_key()
    kw = {"exec_mode": "normal"}

    try:
        # service = client.connect(username="admin", password="pass")
        service = client.connect(token=session_key)
        job = service.jobs.create(spl_query, **kw)
    except Exception as ex:
        raise Exception('error with run jobs to send mail : %s' % email_recipients)


if __name__ == '__main__':
    # email_subject = 'subject'
    # email_message = 'message'
    # email_recipients = 'recipients'
    # email_priority = 'priority'
    # spl_query = (
    #         '| makeresults | sendemail message="%s" subject="%s" to="%s" priority="%s"'
    #         % (email_message, email_subject, email_recipients, email_priority))
    # create_job(spl_query=spl_query)

    # try:
    submit_index(data='data by submit', index_info='iiii')
    # except KeyError as ex:
    #     print(ex)
    # except Exception as ex:
    #     print(ex)