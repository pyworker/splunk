# v.1.0
# coding: utf-8
"""
Module for interaction with the KVStore using API
"""

import json
import otcommon.splunklib.client as client
from collection_api_conf import app
from utils import get_splunk_session_key


class CollectionAPI:
    def __init__(self):
        """
        establish connection with Splunk server
        Raises:
            KeyError: cant connect to Splunk server
        """
        self._collection = None
        try:
            self._service = client.connect(
                token=get_splunk_session_key(),
                app=app,
                owner='nobody'
            )
        except KeyError as ex:
            raise KeyError('cant connect to Splunk server: error: %s' % ex)

    def create(self, collection_name):
        """
        creating a new KVStore collection
        Args:
            collection_name: str, collection name
        Raises:
            Exception: cant create new collection
            KeyError: collection already exists
        Returns:
            pass
        """
        try:
            self._service.kvstore.create(collection_name)
        except Exception as ex:
            raise Exception('error with create collection "%s": "%s"' % (collection_name, ex.message))
        self._collection = self._service.kvstore[collection_name]

    def delete(self):
        """
        deleting a KVStore collection
        Raises:
            KeyError: collection not defined
        Returns:
            True - if collection was deleted
        """
        if not self._collection:
            raise KeyError('collection not defined')
            # raise Exception
        try:
            self._collection.delete()
        except Exception as ex:
            Exception('cant delete collection error %s' % ex)
        return True

    def get(self, collection_name):
        """
        getting an existing KVStore collection
        Args:
            collection_name: str, collection name
        Raises:
            KeyError: collection not defined
        Returns:
            0 - if get collection
        """
        try:
            self._collection = self._service.kvstore[collection_name]
        except KeyError:
            raise KeyError('collection "%s" not defined. use "create()" method' % collection_name)
        return 0

    def delete_entry_by_id(self, key):
        """
        deleting entry from the KVStore by id
        Raises:
            KeyError: collection not defined
            Exception: cant delete entry from KVStore
        Returns:
            pass
        """
        if not self._collection:
            raise KeyError('collection not defined. use "create()" or "get(collection_name)" methods')
        try:
            self._collection.data.delete_by_id(key)
        except Exception as ex:
            raise Exception('cant delete entry from KVStore: error %s' % ex)

    def post_entry(self, entry):
        """
        adding new entry to the KVStore
        Args:
            entry: str, entry adding to the KVStore
        Raises:
            KeyError: invalid type of param
                    : collection not defined
        Returns:
            the key of the entry added to the KVStore
        """

        if isinstance(entry, dict):
            entry = json.dumps(entry)
        elif isinstance(entry, str):
            pass
        else:
            raise KeyError('invalid type of param: "%s" expected "String" or "Dictionary"' % entry)

        if not self._collection:
            raise KeyError('collection not defined. use "create()" or "get(collection_name)" methods')

        res = self._collection.data.insert(entry)
        return res.get('_key')

    def get_entry(self, query):
        """
        obtaining entries from the KVStore collection
        Args:
            query: str or dict, search query
        Raises:
            KeyError: collection not defined
        Returns:
            search result
        """

        if isinstance(query, dict):
            query = json.dumps(query)
            # query = json.dumps({'test_key': {'$ne': 7}})
        elif isinstance(query, str):
            pass
        else:
            raise KeyError('invalid type of query: "%s" expected "String" or "Dictionary"' % query)

        if not self._collection:
            raise KeyError('collection not defined. use "create()" or "get(collection_name)" methods')
        return self._collection.data.query(query=query)

    def delete_entry(self, query):
        """
        deleting entries from collection
        Args:
            query: str or dict, search query
        Raises:
            KeyError: collection not defined
        Returns:
                result of deleting entry in the KVStore collection
                 1 - entry was not deleted
                 0 - entry was deleted
        """
        if not self._collection:
            raise KeyError('collection not defined. use "create()" or "get(collection_name)" methods')

        if isinstance(query, dict):
            query = json.dumps(query)
            # query = json.dumps({'test_key': {'$ne': 7}})
        elif isinstance(query, str):
            pass
        else:
            raise KeyError('invalid type of query: "%s" expected "String" or "Dictionary"' % query)

        # print json.dumps(collection.data.query(query=query))
        result = self._collection.data.delete(query=query)
        result = 0 if int(result.get('status')) == 200 else 1
        return result


if __name__ == '__main__':
    try:
        col = CollectionAPI()
        col.get('tmp_collection')
    except Exception as ex:
        print('catch exception: %s' % ex)

