# v.1.3
"""
Module for administrate and work with splunk users
"""
import otcommon
import otcommon.splunklib.client as client

from collection import Collection
from utils import get_splunk_session_key


class AdminSplunkUser:
    def __init__(self, app='launcher'):
        try:
            self._service = client.connect(
                token=get_splunk_session_key(),
                app=app,
                owner='nobody'
            )
            self._users = self._service.users
            self._rest = Collection()
        except Exception as ex:
            raise Exception('cant connect to Splunk server, error:%s' % ex)

    def create(self, username, password, roles, default_app=None, realname=None, email=None):
        """
        create new Splunk user
        Args:
            username: str, user's username
            password: str, user's password
            roles: str or list, roles granted for user
            default_app: str, application name
            realname: str, user's realname
            email: str, user's email
        Raises:
            Exception: cant create new Splunk user
        Return:
            0 if new user created
        """

        if not default_app:
            default_app = ''

        if not realname:
            realname = ''

        if not email:
            email = ''

        try:
            self._users.create(
                username=username,
                password=password,
                roles=roles,
                defaultApp=default_app,
                realname=realname,
                email=email
            )
        except otcommon.splunklib.binding.HTTPError as http_err:
            raise Exception('cant create new Splunk user, error:"%s"' % http_err.message)
        return 0

    def delete(self, username):
        """
        delete Splunk user
        Args:
            username: str, user's username
        Raises:
            Exception: can't delete user
        Returns:
            0 if user was deleted
        """
        try:
            res = self._users.delete(name=username)
        except otcommon.splunklib.binding.HTTPError as http_ex:
            raise Exception('cant delete user "%s" error: "%s"' % (username, http_ex))
        return 0

    def get(self, username=None):
        """
        get a list of registered users in Splunk
        Return:
            list of Task
        """
        if not username:
            username = ''

        uri = '/services/admin/users/%s?output_mode=json&count=-1' % username
        try:
            splunk_users = self._rest.get_rest_data(uri)
        except Exception as ex:
            raise Exception('error while executing the REST request: %s' % ex.message)

        users_list = []
        for user_passport in splunk_users.get('entry'):
            users_list.append(SplunkUser(user_passport))

        return users_list


class SplunkUser:
    def __init__(self, user):
        """
        parse user passport and fill user fields
        Args:
            user: dict, user description
        Raises:
            None
        """
        if user:
            self._id = user.get('id')
            self._name = user.get('name')
            self._author = user.get('author')
            self._roles = user.get('content').get('roles')
            self._capabilities = user.get('content').get('capabilities')
            self._email = user.get('content').get('email')
            self._realname = user.get('content').get('realname')
            self._user = user

        # except AttributeError as attr_err:
        # raise AttributeError('failed to create new splunk user: %s' % attr_err)

    def get(self):
        """
        get info about user
        Args:

        Raises:
            None
        Returns:
            None
        """
        return {
            'id': self._id,
            'name': self._name,
            'author': self._author,
            'roles': self._roles,
            'capabilities': self._capabilities,
            'email': self._email,
            'realname': self._realname
        }


def create_splunk_user(username, password, roles, default_app=None, real_name=None, email=None):
    """
    create dict with new user
    Args:
        username: the splunk user's name
        password: str, the password
        roles: str or list, a single role or list of roles for the user
        default_app: str, the default app
        real_name: str, user's name
        email: str, user's email address
    Returns:
        user SplunkUser object
    """
    user = {
        'username': username,
        'password': password,
        'roles': roles,
        'default_app': default_app,
        'realname': real_name,
        'email': email
    }
    return SplunkUser(user=user)


if __name__ == '__main__':
    pass
    # try:
    #     asu = AdminSplunkUser()
    #     # users = asu.get(username='test')
    #     users = asu.get()
    #
    #
    #     # asu.delete('test_user')
    #     asu.create(
    #         username='test_user',
    #         password='empty',
    #         roles=['user'],
    #         default_app='alert_manager',
    #         realname='artem',
    #         email='artem@email.com'
    #     )
    #
    #     for user in users:
    #         print user.get().get('name')
    #
    #         # print json.dumps(users, indent=4)
    # except Exception as ex:
    #     print 'catch exception: %s' % ex
