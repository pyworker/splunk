# v.1.0
"""
Module for check server role: captain or sandbox
"""
import socket
from colection_rest import Collection


def verify_server_role(rest):
    """
    check cluster role: captain or sandbox(server not in cluster)
    Args:
        rest: Splunk server handle
    Raises:
        Exception:
            cant connect to server
            the server role is not defined: not captain and not sandbox
    Returns:
        True if server role is captain or sandbox
    """
    host = socket.gethostname()
    sandbox_role = False
    captain_role = None

    uri = '/services/shcluster/status?output_mode=json'

    try:
        captain = rest.get_rest_data(uri=uri)
    except Exception as ex:
        if 'status: 503' in ex.message:
            # not such uri: server not in cluster - it`s sandbox
            sandbox_role = True
            captain = {}
        else:
            raise Exception('cant connect to server : error="%s"')

    if captain != {}:
        for cap in captain.get("entry"):
            captain_role = cap.get("content").get("captain").get("label")
            break

    if not captain_role and not sandbox_role:
        raise Exception('the server role is not defined: not captain and not sandbox')

    if captain_role and captain_role != host:
        raise Exception('current host: %s is not a captain - stop' % host)

    return True


if __name__ == '__main__':
    col = Collection()
    try:
        role = verify_server_role(col)
        print role
    except Exception as ex:
        print 'catch exception: %s' % ex
