# coding=utf-8

import os
import sys

dir = os.path.join(os.path.join(os.environ.get('SPLUNK_HOME')), 'etc', 'apps', 'alert_manager', 'bin', 'lib')
if not dir in sys.path:
    sys.path.append(dir)

from AlertManagerLogger import setupLogger

if __name__ == '__main__':

    log = setupLogger('alert_manager')
    try:
        log.info('operation="start module %s"' % __file__)
        if len(sys.argv) < 2 or sys.argv[1] != '--execute':
            log.fatal('Unsupported execution mode (expected --execute flag)')
            raise VallueError('error=invalid params len=2 but get=%d params:%s ' % (len(sys.argv), sys.argv))
    except Exception as ex:
        log.error('error="%s" stop module %s' % (ex, __file__))
    finally:
        log.info('operation="end module %s"' % __file__)
